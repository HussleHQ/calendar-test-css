# Calendar

The task is to create a demo calendar application using HTML and CSS. No JavaScript is expected.

1. Use static HTML and CSS only.
1. Keep everything in the index.html file, to keep things simple.
1. We are not expecting any functionality to work - we want to see the button to add a new event, for example, but we're not expecting the button to do anything when we click on it.
1. We're not testing your design skills. We want to see how you implement ideas in HTML / CSS.

We suggest you start by rendering a single month view of a calendar, similar to the image below.

![picture](img/calendar-aug-2018.jpg)

##Features & Requirements:
1. Style the existing content to make the calendar display similar to the image shown.
1. Add events (calendar events, not JavaScript events!) to some of the dates on the calendar. Make sure some dates have more than one event. Events should have a title and a time.
1. Add a longer text description to some of your events. The event display should get bigger to show the description when the user's mouse hovers over the event. The rest of the time, the description shouldn't be visible.
1. As well as making the description visible, a mouse hover should also show buttons to edit, duplicate and delete the event. Remember, we don't expect the buttons to work.
1. Add styling so that the work days (Monday to Friday) are displayed with a different background colour on the calendar.
1. Users can now add labels to their calendar events, which should be added as additional CSS classes. Different labels should cause the events to be shown with different colours (that could be a background or text colour, or a stripe of colour on the event).
1. Add so many calendar events to one day that they overflow the space available. Make sure the events don't display on top of other content in the calendar.
1. Add buttons that would allow for adding events and navigating to other months in the calendar.

Please don't spend more than 2 hours on this. Start at the the top of the features and requirements, working your way down getting as many done as possible within that timeframe.

## Starting

Please clone this repository and write your code by modifying the index.html file.

## Submission

Send us a link to the repository on a shared repository system like Github or BitBucket (which supports free private repositories, if that's of interest).


